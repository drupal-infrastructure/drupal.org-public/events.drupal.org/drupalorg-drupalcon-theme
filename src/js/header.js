var large  = 1441,
    normal = 1200,
    medium = 978,
    small  = 500;

(function ($) {

  Drupal.behaviors.header = {
    attach: function (context, settings) {

      // Navigation.
      $('#btn-open-nav').click(function (e) {
        $('body').toggleClass('open-menu-mobile');
        $(this).toggleClass('is-active');
      });

      if (matchMedia && !$('body').hasClass('drupalcon-width-processed')) {
        const mq = window.matchMedia('(max-width: 1199px)');
        $('body').addClass('drupalcon-width-processed');
        mq.addEventListener('change', WidthChange);
        WidthChange(mq);
      }

      function WidthChange(mq) {
        if (mq.matches) {
            small();
        } else {
            large();
        }
        header();
      }

      function small() {

        $('.region-header').prepend($('.header-secondary'));

        var $mainMenuEl = $('.region-header .menu > li > a');

        $mainMenuEl.each(function () {
          if ($(this).next().find("h3").length === 0) {
              var title = $(this)[0].outerHTML;
              $(this).next().prepend("<li class='mobile-util'><h3>" + title + "</h3><button class='nav-back'><span>" + Drupal.t('Back') + "</span></button></li>");
          }
          if ($(this).parent().hasClass('menu-item--expanded') && $(this).find('.open-submenu').length === 0) {
              $(this).append('<button class="open-submenu mobile-util" aria-haspopup="true"></button>');
          }

          $('.nav-back').click(function (e) {
            $(this).closest('.menu').removeClass('active');
          });
          
          $mainMenuEl.find('.open-submenu').click(function (e) {
              $(this).parent().next().addClass('active');
              e.preventDefault();
          });

          $('.l-mobile-header').addClass('h-header-active');
          $('header').removeClass('h-header-active');

        });

      }

      function large() {

        $('.header-secondary').insertAfter($('.header-main'));
        $('.mobile-util').remove();
        $('body').removeClass('open-menu-mobile');

        $('header').addClass('h-header-active');
        $('.l-mobile-header').removeClass('h-header-active');

      }

      function header() {
        var $header = $('.h-header-active');

        if ($('body.path-frontpage').length === 1) {
          var headerHeight = 0;
        } else {
          var headerHeight = $header.outerHeight();
        }

        $('main').css({"padding-top": headerHeight});

      }

    }
  };
}(jQuery));
