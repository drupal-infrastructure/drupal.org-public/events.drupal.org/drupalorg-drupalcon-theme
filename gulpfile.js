// include gulp & configuration
var gulp = require('gulp');
var config = require('./gulp_config.json');

// include plug-ins
var $ = require('gulp-load-plugins')({
  rename: {
    'gulp-sass-bulk-import': 'bulkSass'
  }
});

// Watch.
gulp.task('watch', function() {
  gulp.watch(config.scss.files, ['scss']);
  gulp.watch(config.images.files, ['images']);
  gulp.watch(config.scripts.files, ['scripts']);
  gulp.watch(config.scripts.files, ['json']);
});

// Sass.
gulp.task('scss', function() {
  gulp.src(config.scss.files)
  .pipe($.bulkSass())
  .pipe($.sourcemaps.init())
  .pipe($.sass({
    includePaths: ['./node_modules/breakpoint-sass/stylesheets']
  }).on('error', $.notify.onError({
    message: "File: <%= error.message %>",
    title: "SCSS task / Error"
  })))
  .pipe($.autoprefixer('last 10 versions'))
  .pipe($.cssnano())
  .pipe($.sourcemaps.write('./maps'))
  .pipe(gulp.dest(config.scss.dest));
});

// JS.
gulp.task('scripts', function() {
  gulp.src(config.scripts.files)
  .pipe($.sourcemaps.init())
  .pipe($.concat('main.min.js'))
  .pipe($.terser().on('error', $.notify.onError({
    message: "File: <%= error.message %>",
    title: "Scripts task / Error"
  })))
  .pipe($.sourcemaps.write('maps'))
  .pipe(gulp.dest(config.scripts.dest));
});

// JSON.
gulp.task('json', function() {
  gulp.src(config.json.files)
  .pipe($.sourcemaps.init())
  .pipe($.sourcemaps.write('maps'))
  .pipe(gulp.dest(config.json.dest));
});

// Utils.
gulp.task('vendors', function() {
  gulp.src(config.vendors.files)
  .pipe($.concat('vendors.min.js'))
  .pipe($.uglify())
  .pipe(gulp.dest(config.vendors.dest));
});

// Images.
gulp.task('images', function() {
  gulp.src(config.images.files)
  .pipe($.imagemin())
  .pipe(gulp.dest(config.images.dest));
});

gulp.task('default', ['scss', 'images', 'scripts', 'json']);
